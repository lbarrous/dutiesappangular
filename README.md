# Dutiesapp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.6.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

Once you launch the project you will se an SPA which is connected to a REST API which is in charge of managing the different duties.

A page with an input will be displayed, in this input you can type your duty, and it will be saved once you click on submit.

Then, you can edit that duty or remove it just in case.

*IMPORTANT* To use this app is necessary to launch the server, located in: https://bitbucket.org/lbarrous/dutiesserverexpress/src/develop